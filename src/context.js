// context Api is actual provider class
//we wrap around this class to the entire application
// all compoment wrap around this provider class
import React, { Component } from 'react';
import axios from 'axios';
const Context = React.createContext();
//action is object with a type(capitlize string) submit action when we click delete
//payload is just a date send along with action to be applied
// in case of delete it is id of that Object
// in case on add new contact payload is newContact Object
const reducer = (state, action) => {
	switch (action.type) {
		case 'DELETE_CONTACT':
			return {
				...state,
				contacts: state.contacts.filter(
					contact => contact.id !== action.payload
				)
			};
		case 'ADD_CONTACT':
			return {
				...state,
				contacts: [action.payload, ...state.contacts]
			};
		case 'UPDATE_CONTACT':
			return {
				...state,
				contacts: state.contacts.map(contact =>
					contact.id === action.payload.id
						? (contact = action.payload)
						: contact
				)
			};
		default:
			return state;
	}
};
export class Provider extends Component {
	// in this provider we have a global state

	state = {
		contacts: [],
		dispatch: action => {
			this.setState(state => reducer(state, action));
		}
	};

	// componentDidMount() {
	// 	axios
	// 		.get('https://jsonplaceholder.typicode.com/users')
	// 		.then(res => this.setState({ contacts: res.data }));
	// }
	//second way to do aync ajax call to context api

	// await has to wait to finish the aync ajax call
	async componentDidMount() {
		const res = await axios.get('https://jsonplaceholder.typicode.com/users');
		this.setState({ contacts: res.data });
	}

	render() {
		return (
			<Context.Provider value={this.state}>
				{this.props.children}
			</Context.Provider>
		);
	}
}

//export the consumer now which use the provider

export const Consumer = Context.Consumer;
