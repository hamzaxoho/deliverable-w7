import React, { Component } from 'react';
import propTypes from 'prop-types';
import { Consumer } from '../../context';
import { Link } from 'react-router-dom';
import axios from 'axios';
//stateful component
class Contact extends Component {
	state = {
		showContactInfo: false
	};
	// static propTypes = {
	// 	name: propTypes.string.isRequired,
	// 	email: propTypes.string.isRequired,
	// 	phone: propTypes.string.isRequired
	// };
	// constructor() {
	// 	super();
	// 	state = {};
	// 	this.onShowClick = this.onShowClick.bind(this);
	// }

	// set the state of contact component as toggle case
	onShowClick = e => {
		this.setState({ showContactInfo: !this.state.showContactInfo });
	};
	// onDeleteClick = (id, dispatch) => {
	// 	// this.props.deleteClickHandler();

	// 	axios
	// 		.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
	// 		.then(res => dispatch({ type: 'DELETE_CONTACT', payload: id }));
	// };

	onDeleteClick = async (id, dispatch) => {
		await axios
			.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
			.then(res => dispatch({ type: 'DELETE_CONTACT', payload: id }));
	};

	render() {
		const { id, name, email, phone } = this.props.contact;
		const { showContactInfo } = this.state;

		return (
			<Consumer>
				{value => {
					const { dispatch } = value;
					return (
						<div className="card card-body mb-3">
							<h4>
								{name}{' '}
								<i
									onClick={this.onShowClick}
									className="fas fa-sort-down"
									style={{ cursor: 'pointer' }}
								/>
								<i
									onClick={this.onDeleteClick.bind(this, id, dispatch)}
									className="fas fa-times"
									style={{
										cursor: 'pointer',
										float: 'right',
										color: 'red'
									}}
								/>
								<Link to={`contact/edit/${id}`}>
									<i
										className="fas fa-pencil-alt"
										style={{
											cursor: 'pointer',
											color: 'black',
											float: 'right',
											marginRight: '16px'
										}}
									/>
								</Link>
							</h4>
							{showContactInfo ? (
								<ul className="list-group">
									<li className="list-group-item">Email:{email} </li>
									<li className="list-group-item">Pnone: {phone}</li>
								</ul>
							) : null}
						</div>
					);
				}}
			</Consumer>
		);
	}
}

Contact.propTypes = {
	contact: propTypes.object.isRequired
	// deleteClickHandler: propTypes.func.isRequired
};

export default Contact;
