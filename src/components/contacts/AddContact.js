import React, { Component } from 'react';
import { Consumer } from '../../context';
import TextInputGroup from '../layout/TextInputGroup';
import axios from 'axios';

//stateful component
class AddContact extends Component {
	state = {
		name: '',
		email: '',
		phone: '',
		errors: {}
	};

	onSubmit = async (dispatch, e) => {
		e.preventDefault();
		// get values from state using destructuring
		const { name, email, phone } = this.state;
		//check for Errors
		if (name === '') {
			this.setState({ errors: { name: 'Name is Required' } });
			return;
		}
		if (email === '') {
			this.setState({ errors: { email: 'Email Feild must not be empty' } });
			return;
		}
		if (phone === '' && phone.length < 11) {
			this.setState({ errors: { phone: 'Phone is Number is Invalid' } });
			return;
		}
		//construct a new contact object
		const newContact = {
			name: name,
			email: email,
			phone: phone
		};

		const res = await axios.post(
			'https://jsonplaceholder.typicode.com/users/',
			newContact
		);
		dispatch({ type: 'ADD_CONTACT', payload: res.data });

		// clear State
		this.setState({
			name: '',
			email: '',
			phone: '',
			errors: {}
		});

		// use to get home page back after adding a new contact
		this.props.history.push('/');
	};
	onChange = e => this.setState({ [e.target.name]: e.target.value });

	render() {
		const { name, email, phone, errors } = this.state;
		return (
			<Consumer>
				{value => {
					const { dispatch } = value;
					return (
						<div className="card mb-3">
							<div className="card-header">Add Contact</div>
							<div className="card-body">
								<form onSubmit={this.onSubmit.bind(this, dispatch)}>
									<TextInputGroup
										label="Name"
										name="name"
										placeholder="Enter Name.."
										value={name}
										onChange={this.onChange}
										error={errors.name}
									/>

									<TextInputGroup
										label="Email"
										name="email"
										type="email"
										placeholder="Enter Email.."
										value={email}
										onChange={this.onChange}
										error={errors.email}
									/>
									<TextInputGroup
										label="Phone"
										name="phone"
										type="text"
										placeholder="Enter Phone.."
										value={phone}
										onChange={this.onChange}
										error={errors.phone}
									/>
									<input
										type="submit"
										value="Add Contact"
										className="btn btn-light btn-block"
									/>
								</form>
							</div>
						</div>
					);
				}}
			</Consumer>
		);
	}
}

export default AddContact;
