import React, { Component } from 'react';
import Contact from './Contact';
import { Consumer } from '../../context';

//stateless component it consumes the state from provider class in context api
class Contacts extends Component {
	// deleteContact = id => {
	// 	const { contacts } = this.state;
	// 	const newContacts = contacts.filter(contact => contact.id !== id);
	// 	this.setState({
	// 		contacts: newContacts
	// 	});
	// };
	render() {
		return (
			<Consumer>
				{value => {
					return (
						<React.Fragment>
							<h1 className="display-4">
								<span className="text-danger">Contact</span> List
							</h1>
							{value.contacts.map(contact => (
								<Contact
									key={contact.id}
									contact={contact}
									// deleteClickHandler={this.deleteContact.bind(this, contact.id)}
								/>
							))}
						</React.Fragment>
					);
				}}
			</Consumer>
		);
	}
}

export default Contacts;
