import React from 'react';

export default function About() {
	return (
		<>
			<h1 className="display-4">About Contact Manger</h1>
			<p className="lead">Simple Page for Contact Manager Application </p>
			<p className="text-secondary">Version 1.0.0</p>
		</>
	);
}
