import React, { Component } from 'react';
class Test extends Component {
	state = {
		title: '',
		body: ''
	};
	//http request fetch data from api or ajax call are write in component did mount
	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/posts/1')
			.then(response => response.json())
			.then(jsondata =>
				this.setState({
					title: jsondata.title,
					body: jsondata.body
				})
			);
	}

	// componentWillMount() {
	// 	console.log('Component Will Mount..');
	// }

	// componentDidUpdate() {
	// 	console.log('Component Did Update.');
	// }
	// componentWillUpdate() {
	// 	console.log('Component Will Update..');
	// }
	// componentWillUnmount() {
	// 	console.log('Component Will UnMount..');
	// }
	// componentDidCatch() {
	// 	console.log('Component Did catch..');
	// }

	// //depreciate in the presence of
	// //component will mount and will update

	// componentWillReceiveProps(nextProp, nextState) {
	// 	console.log('Component will recieve Props..');
	// }

	// static getDerivedStateFromProps(nextProps, prevState) {
	// 	return {
	// 		test: 'something'
	// 	};
	// }
	// getSnapshotBeforeUpdate(prevProps, prevState) {
	// 	console.log('Component getSnapshotBeforeUpdate..');
	// }

	render() {
		const { title, body } = this.state;
		return (
			<div>
				<h1>Test Component Life Cycle</h1>
				<p>
					Title:
					<br />
					{title}
				</p>
				<p>
					Body:
					<br />
					{body}
				</p>
			</div>
		);
	}
}

export default Test;
