import React, { Component } from 'react';
import propTypes from 'prop-types';
class Contact extends Component {
	state = {
		showContactInfo: true
	};
	// static propTypes = {
	// 	name: propTypes.string.isRequired,
	// 	email: propTypes.string.isRequired,
	// 	phone: propTypes.string.isRequired
	// };
	// constructor() {
	// 	super();
	// 	state = {};
	// 	this.onShowClick = this.onShowClick.bind(this);
	// }

	onShowClick = e => {
		this.setState({ showContactInfo: !this.state.showContactInfo });
	};
	onDeleteClick = () => {
		this.props.deleteClickHandler();
	};
	render() {
		const { name, email, phone } = this.props.contact;
		const { showContactInfo } = this.state;
		return (
			<div className="card card-body mb-3">
				<h4>
					{name}{' '}
					<i
						onClick={this.onShowClick}
						className="fas fa-sort-down"
						style={{ cursor: 'pointer' }}
					/>
					<i
						onClick={this.onEditClick}
						className="fas fa-times"
						style={{ cursor: 'pointer', float: 'right', color: 'red' }}
					/>
					<i
						onClick={this.onDeleteClick}
						className="fas fa-times"
						style={{ cursor: 'pointer', float: 'right', color: 'red' }}
					/>
				</h4>
				{showContactInfo ? (
					<ul className="list-group">
						<li className="list-group-item">Email:{email} </li>
						<li className="list-group-item">Pnone: {phone}</li>
					</ul>
				) : null}
			</div>
		);
	}
}

Contact.propTypes = {
	contact: propTypes.object.isRequired,
	deleteClickHandler: propTypes.func.isRequired
};

export default Contact;
